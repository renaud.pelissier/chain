# Chains API
Smart pipeline processing API for ordered series of objects.

### Details 
You are working with ordered and finite sequences of items : _**Chains**_.  
Chains must pass through a processing _**Pipeline**_ of multiple steps.  
Processing should be retried when failed with a certain delay strategy. 
Processing should be triggered only when certain external _**Condition**_ are met.
A step should process items as available not waiting for previous step to finish.
 
**Example** : Multi step Image processing using local and cloud processing.


### Features :
* every step of the Pipeline running in parallel
* handle processing errors with retry strategies
* handle processing conditions to avoid useless retry and trigger restart asap
* persistence and caching with eviction strategy
* stop and resume anytime with no data loss
* resume from where it has stopped, not restarting the whole pipeline
* java/kotlin JDK & Android compatible
* almost pure implementation very light dependencies
* low memory and CPU footprint
* scalable using non-blocking threading
* no multi-threading skills required

### Code example

This is a main example using ```String``` processing.  
For further details, you can explore the Unit tests.

````kotlin
package com.pupscan.chains

import com.pupscan.chains.chain.BufferChain
import com.pupscan.chains.chain.Chain
import com.pupscan.chains.chain.generic
import com.pupscan.chains.condition.Condition
import com.pupscan.chains.condition.VerifiedCondition
import com.pupscan.chains.pipeline.Pipeline
import com.pupscan.chains.pipeline.PipelineExecutor
import com.pupscan.chains.processor.SegmentProcessor
import com.pupscan.chains.processor.SegmentProcessorTest
import org.junit.Test
import org.slf4j.LoggerFactory
import java.lang.StringBuilder
import java.util.concurrent.Semaphore
import kotlin.random.Random

class Example {

    val log = LoggerFactory.getLogger(javaClass)
    val STEP1 = "STEP1"
    val STEP2 = "STEP2"
    val steps = listOf(STEP1, STEP2)

    @Test
    fun test() {
        //Will trigger the shutdown on Pipeline success.
        val shutdownPermit = Semaphore(0)

        //Observe the progress of a step of the Pipeline
        val stepObserver = { pipeline: Pipeline, step: String ->
            val processor = pipeline.processorFor(step)
            log.info("Observing : $processor")
        }

        //Handle the success of the last step of the Pipeline
        val endHandler = {
            shutdownPermit.release()
            log.info("Observing : SUCCESS")
        }

        //Our Pipeline components
        //We have 2 steps, so we need 3 Chain and 2 SegmentProcessor
        val id = "pipeline1"
        val chains = listOf(
            newChain(),
            newChain(),
            newChain()
        )
        val processors = listOf(
            processor(),
            processor()
        )

        //Here you can define the Conditions that should be verified before triggering execution of each step.
        //Example : network is up, some other resource is available...
        val conditions = listOf<Condition>(VerifiedCondition(), VerifiedCondition())

        //Create the Pipeline : chain[0] -> STEP1 / processor[0] -> chain[1] -> STEP2 / processor[1] -> chain[2]
        val pipeline = Pipeline(
            steps,
            chains,
            processors,
            endHandler,
            stepObserver,
            conditions
        )

        //Will handle the execution of every Pipeline.
        val pipelineExecutor = PipelineExecutor()

        //Submit our Pipeline
        pipelineExecutor.begin(id, pipeline)

        //Feed the Pipeline (this is equivalent to adding to 'chain0')
        for (i in 1..15)
            pipelineExecutor.add(id, "value$i")
        //End the Pipeline (this is equivalent to ending 'chain0')
        pipelineExecutor.end(id)

        //Use our semaphone to wait for the success
        shutdownPermit.acquire()

        //Shutdown our executor.
        pipelineExecutor.shutdown()
    }

    private fun newChain(): Chain<Any> {
        //Create an in-memory (not persisted) Chain<String> and cast it to Chain<Any> by invoking '.generic()'
        return BufferChain("end").generic()
    }

    private fun processor(): SegmentProcessor {
        //This is our test implementation of a processor. In our case will concatenate up to 2 strings and wait for
        //a random duration so we have a chance to see that both steps are running in parallel.
        return object : SegmentProcessor {
            override fun process(source: Chain<*>, sourceTimeoutMs: Long): Pair<Int, List<Any>> {
                val items = mutableListOf<String>()
                for (i in 0..Random.nextInt(2)) {
                    val node = source.read(sourceTimeoutMs) ?: break
                    items.add(node.lazy.value.toString())
                }
                Thread.sleep(Random.nextLong(50))
                if (items.isEmpty())
                    return 0 to emptyList()
                else
                    return items.size to listOf(items.joinToString(", "))
            }
        }
    }
}
````
Output.  
Notice that :
* both steps are running in parallel.
* the total number for STEP2 is evolving according to what is available from STEP1.

````shell
18:11:02.351 DEBUG PipelineExecutor:49 - Submitting Pipeline[578962046][STEP1]
18:11:02.364 DEBUG StepExecutor:55 - [STEP1] Submit ChainProcessor[STEP1][WAITING][0/0].
18:11:02.385 DEBUG PipelineExecutor:49 - Submitting Pipeline[578962046][STEP2]
18:11:02.386 DEBUG StepExecutor:62 - [STEP1] Running ChainProcessor[STEP1][WAITING][0/0].
18:11:02.388 INFO  Example:33 - Observing : ChainProcessor[STEP1][RUNNING][0/0]
18:11:02.398 DEBUG StepExecutor:55 - [STEP2] Submit ChainProcessor[STEP2][WAITING][0/0].
18:11:02.400 DEBUG StepExecutor:62 - [STEP2] Running ChainProcessor[STEP2][WAITING][0/0].
18:11:02.401 INFO  Example:33 - Observing : ChainProcessor[STEP2][RUNNING][0/0]
18:11:02.474 INFO  Example:33 - Observing : ChainProcessor[STEP1][RUNNING][1/15]
18:11:02.506 INFO  Example:33 - Observing : ChainProcessor[STEP1][RUNNING][3/15]
18:11:02.533 INFO  Example:33 - Observing : ChainProcessor[STEP2][RUNNING][2/2]
18:11:02.538 INFO  Example:33 - Observing : ChainProcessor[STEP1][RUNNING][4/15]
18:11:02.552 INFO  Example:33 - Observing : ChainProcessor[STEP1][RUNNING][6/15]
18:11:02.565 INFO  Example:33 - Observing : ChainProcessor[STEP1][RUNNING][8/15]
18:11:02.595 INFO  Example:33 - Observing : ChainProcessor[STEP1][RUNNING][9/15]
18:11:02.627 INFO  Example:33 - Observing : ChainProcessor[STEP1][RUNNING][11/15]
18:11:02.636 INFO  Example:33 - Observing : ChainProcessor[STEP1][RUNNING][12/15]
18:11:02.665 INFO  Example:33 - Observing : ChainProcessor[STEP1][RUNNING][14/15]
18:11:02.682 INFO  Example:33 - Observing : ChainProcessor[STEP2][RUNNING][3/9]
18:11:02.694 INFO  Example:33 - Observing : ChainProcessor[STEP1][RUNNING][15/15]
18:11:02.694 INFO  Example:33 - Observing : ChainProcessor[STEP1][SUCCESS][15/15]
18:11:02.700 DEBUG StepExecutor:75 - [STEP1] End Handling ChainProcessor[STEP1][SUCCESS][15/15].
18:11:02.705 INFO  Example:33 - Observing : ChainProcessor[STEP2][RUNNING][5/10]
18:11:02.739 INFO  Example:33 - Observing : ChainProcessor[STEP2][RUNNING][6/10]
18:11:02.771 INFO  Example:33 - Observing : ChainProcessor[STEP2][RUNNING][7/10]
18:11:02.794 INFO  Example:33 - Observing : ChainProcessor[STEP2][RUNNING][9/10]
18:11:02.828 INFO  Example:33 - Observing : ChainProcessor[STEP2][RUNNING][10/10]
18:11:02.829 INFO  Example:33 - Observing : ChainProcessor[STEP2][SUCCESS][10/10]
18:11:02.829 INFO  Example:39 - Observing : SUCCESS
18:11:02.833 DEBUG StepExecutor:75 - [STEP2] End Handling ChainProcessor[STEP2][SUCCESS][10/10].
18:11:02.843 DEBUG StepExecutor:35 - [STEP1] Shutdown initiated.
18:11:02.848 DEBUG StepExecutor:35 - [STEP2] Shutdown initiated.
18:11:02.849 DEBUG StepExecutor:48 - [STEP1] Waiting for Processors to finish.
18:11:02.850 DEBUG StepExecutor:50 - [STEP1] Terminated
18:11:02.850 DEBUG StepExecutor:48 - [STEP2] Waiting for Processors to finish.
18:11:02.851 DEBUG StepExecutor:50 - [STEP2] Terminated
````

### Android integration

For Android, you will need to replace the Guava dependency as following :

````groovy
implementation('com.pupscan:chain:x.y.z') {
        exclude group: 'com.google.guava', module: 'guava'
}
implementation 'com.google.guava:guava:27.1-android'
````

©rpelissier - June 2019