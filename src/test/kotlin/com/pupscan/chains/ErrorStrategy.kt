package com.pupscan.chains

import java.lang.Exception
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.random.Random

interface ErrorStrategy {
    fun throwError(nextIndex: Int)

    class Never : ErrorStrategy {
        override fun throwError(nextIndex: Int) {
        }
    }

    class Always(val index: Int, val exception: Exception) : ErrorStrategy {
        override fun throwError(nextIndex: Int) {
            if (nextIndex >= index) {
                throw exception
            }
        }
    }

    class OnlyOnce(val index: Int, val exception: Exception) : ErrorStrategy {
        private var firstConditionMatch = AtomicBoolean(true)
        override fun throwError(nextIndex: Int) {
            if (nextIndex >= index && firstConditionMatch.getAndSet(false)) {
                throw exception
            }
        }
    }

    class Randomly(var probability: Float, vararg exceptions: Exception) : ErrorStrategy {
        private val exceptions = exceptions.asList()

        override fun throwError(nextIndex: Int) {
            if (Random.nextFloat() < probability) {
                val randomIndex = Random.nextInt(0, exceptions.size)
                throw exceptions[randomIndex]
            }
        }
    }
}