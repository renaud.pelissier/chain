package com.pupscan.chains.chain

import com.pupscan.chains.store.MemoryStore
import org.junit.Assert
import org.junit.Test

class LazyLoadingChainTest : AbstractStoredChainTest(factory) {

    companion object {
        val indexStore = MemoryStore<Chain.Node<String>>()
        val itemStore = MemoryStore<String>()

        val factory = {
            LazyLoadingChain(indexStore, itemStore)
        }
    }


    @Test
    fun testAutoRecycleAtEnd() {

        val chain = chainFactory() as LazyLoadingChain
        chain.autoRecycle = true

        for (i in 1..10)
            chain.add(i, 1, "Item$i")

        chain.end()


        Assert.assertTrue(indexStore.list().isEmpty())
        Assert.assertTrue(itemStore.list().isEmpty())

    }
}