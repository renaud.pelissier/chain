package com.pupscan.chains.chain

import org.junit.Assert
import org.junit.Test
import java.lang.IllegalStateException

class RecycleLockTest {

    @Test
    fun test() {
        val chain = BufferChain<String>("END_OF_CHAIN")
        val lock = RecycleLock(chain)

        lock.ensureNotRecycled()

        Assert.assertFalse(lock.getAndSetRecycled())

        try {
            lock.ensureNotRecycled()
            Assert.fail("IllegalStateException expected.")
        } catch (e: IllegalStateException) {
        }

        Assert.assertTrue(lock.getAndSetRecycled())

        try {
            lock.ensureNotRecycled()
            Assert.fail("IllegalStateException expected.")
        } catch (e: IllegalStateException) {
        }
    }
}