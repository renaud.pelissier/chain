package com.pupscan.chains.chain

class BufferChainTest : AbstractChainTest({ BufferChain(valueEnd) }) {

    companion object {
        val valueEnd = "END_OF_CHAIN"
    }
}