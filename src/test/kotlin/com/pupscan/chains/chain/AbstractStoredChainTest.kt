package com.pupscan.chains.chain

import org.junit.Assert
import org.junit.Test

abstract class AbstractStoredChainTest(statefulChainFactory: () -> Chain<String>) : AbstractChainTest(statefulChainFactory) {

    @Test
    fun `Trim is restored after restarting a chain from the same store`() {

        val chain = chainFactory()

        for (i in 1..10)
            chain.add(i, 1, "Item$i")

        val trimCount = 5
        chain.readPosition(trimCount)
        chain.trim(trimCount)

        //New Chain with the same store
        val chain2 = chainFactory()

        Assert.assertEquals(chain.trimCount, chain2.trimCount)
        Assert.assertEquals(chain.count, chain2.count)

        chain.recycle()
    }

    @Test
    fun <T> `Data and isEnded are restored after resuming`() {

        val chain1 = chainFactory()
        val firsCount = 10
        val secondCount = 20
        (0 until firsCount).forEach { index ->
            chain1.add(index, 1, provider(index.toString()))
        }

        val chain2 = chainFactory()

        Assert.assertEquals(firsCount, chain2.count)
        Assert.assertFalse(chain2.isEnded())

        (firsCount until secondCount).forEach { index ->
            chain2.add(index, 1, provider(index.toString()))
        }

        chain2.end()

        val chain3 = chainFactory()

        Assert.assertEquals(secondCount, chain3.count)
        Assert.assertTrue(chain3.isEnded())

        chain3.sequence().forEach {
            println(it)
        }

        chain3.recycle()
    }
}