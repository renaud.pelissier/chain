package com.pupscan.chains.processor

import com.pupscan.chains.ErrorStrategy
import com.pupscan.chains.SleepStrategy
import com.pupscan.chains.chain.Chain
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.random.Random

class SegmentProcessorTest {

    class BlockableAppenderProcessor(
        maxNLine: Int,
        sleepStrategy: SleepStrategy = SleepStrategy.Never(),
        errorStrategy: ErrorStrategy = ErrorStrategy.Never()
    ) : RandomLineAppenderProcessor(maxNLine, sleepStrategy, errorStrategy) {

        @Volatile
        var blocked = false

        override fun process(source: Chain<*>, sourceTimeoutMs: Long): Pair<Int, List<Any>> {
            if (blocked)
                return 0 to emptyList()
            else
                return super.process(source, sourceTimeoutMs)
        }
    }

    open class RandomLineAppenderProcessor(
        val maxNLine: Int,
        val sleepStrategy: SleepStrategy = SleepStrategy.Never(),
        val errorStrategy: ErrorStrategy = ErrorStrategy.Never()
    ) : SegmentProcessor {
        override fun process(source: Chain<*>, sourceTimeoutMs: Long): Pair<Int, List<Any>> {
            val maxCount = Random.nextInt(1, maxNLine + 1)
            val readSourceItems = mutableListOf<String>()
            val firstIndex = source.readPosition
            for (i in 1..maxCount) {
                val node = source.read(sourceTimeoutMs) ?: break
                readSourceItems.add(node.lazy.value as String)
                errorStrategy.throwError(source.readPosition)
                sleepStrategy.sleep(source.readPosition)
            }
            if (readSourceItems.size > 0) {
                return readSourceItems.size to listOf(
                    readSourceItems.joinToString(
                        "\n",
                        prefix = "[$firstIndex->${firstIndex + readSourceItems.size - 1}]"
                    )
                )
            } else
                return 0 to emptyList()

        }
    }


}