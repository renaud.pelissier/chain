package com.pupscan.chains.processor

import org.junit.Assert
import org.junit.Test
import org.slf4j.LoggerFactory

class ParallelProcessorTest {

    private val log = LoggerFactory.getLogger(javaClass)

    @Test
    fun testTimeout() {

        val id = "id"
        val count = 40
        val innerProcessor = OneToOneProcessorTest.DecoratingOneToOneProcessor()
        val processor = ChainProcessor(
            "$id-step",
            ChainProcessorTest.sourceChain(id, count),
            ChainProcessorTest.targetChain(),
            ParallelProcessor(innerProcessor, 4),
            { log.debug(it.toString()) }
        )

        ChainProcessor.sourceReadTimeOutMs = 5
        processor.run()

        Assert.assertEquals(count, processor.progress.completed)
        Assert.assertEquals(ChainProcessor.State.SUCCESS, processor.state)
        Assert.assertTrue(processor.progress.exception == null)
    }
}