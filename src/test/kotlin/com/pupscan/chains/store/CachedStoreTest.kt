package com.pupscan.chains.store

class CachedStoreTest : AbstractStoreTest<String>(store, stringIdProvider, stringComparator) {

    companion object {
        val store = MemoryStore<String>().cached()
    }
}