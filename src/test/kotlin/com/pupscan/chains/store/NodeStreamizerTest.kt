package com.pupscan.chains.store

import com.pupscan.chains.chain.Chain
import com.pupscan.chains.stream.NodeStreamizer
import com.pupscan.chains.stream.StringStreamizer
import org.junit.Assert
import org.junit.Test

class NodeStreamizerTest {

    @Test
    fun test() {
        val streamizer = NodeStreamizer(StringStreamizer())
        val node = Chain.Node(6424, 3434, lazy { "test" })

        val node2 = streamizer.unstream(streamizer.stream(node))

        Assert.assertEquals(node.lazy.value, node2.lazy.value)
        Assert.assertEquals(node.originIndex, node2.originIndex)
        Assert.assertEquals(node.originCount, node2.originCount)
    }
}