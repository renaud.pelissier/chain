package com.pupscan.chains.store

import com.pupscan.chains.TestUtils
import java.io.InputStream

class DiskStreamStoreTest : AbstractStoreTest<List<InputStream>>(store, provider, comparator) {

    companion object {
        val store = DiskStreamStore(TestUtils.testDirectory(DiskStreamStoreTest::class.java), listOf(".txt", ".txt"))
        val provider = { id: String ->
            listOf("First$id".byteInputStream(), "Second$id".byteInputStream())
        }
        val comparator = Comparator { t: List<InputStream>, u: List<InputStream> ->
            if (t.size != u.size)
                t.size - u.size
            else {
                var diff = 0
                for (i in 0..t.lastIndex) {
                    val tMd5 = Utils.md5(t[i].readAllBytes())
                    val uMd5 = Utils.md5(u[i].readAllBytes())
                    diff = tMd5.compareTo(uMd5)
                    if (diff != 0)
                        break
                }
                diff
            }
        }
    }
}