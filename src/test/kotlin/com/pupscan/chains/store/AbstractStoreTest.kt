package com.pupscan.chains.store

import org.junit.Assert
import org.junit.Test
import java.util.Comparator

abstract class AbstractStoreTest<T>(
    private val store: Store<T>,
    private val idProvider: (String) -> T,
    private val comparator: Comparator<T>
) {

    companion object {
        val stringIdProvider = { id: String -> "Value$id" }
        val stringComparator = kotlin.Comparator { t: String, u: String -> t.compareTo(u) }
    }

    @Test
    fun having_added_items_assert_can_read_and_delete() {
        val count = 10
        (0 until count).forEach { index ->
            store.put(index.toString(), idProvider(index.toString()))
        }

        Assert.assertEquals(10, store.list().count())

        (0 until count).forEach { index ->
            val value = store.get(index.toString())
            Assert.assertTrue(comparator.compare(idProvider(index.toString()), value) == 0)
            println("${index} : $value")
        }

        store.delete(0.toString())

        Assert.assertEquals(9, store.list().count())

        store.clear()

        Assert.assertEquals(0, store.list().count())
    }
}