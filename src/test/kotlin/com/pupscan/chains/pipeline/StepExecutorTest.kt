package com.pupscan.chains.pipeline

import com.pupscan.chains.ErrorStrategy
import com.pupscan.chains.SleepStrategy
import com.pupscan.chains.condition.VerifiedCondition
import com.pupscan.chains.processor.ChainProcessor
import com.pupscan.chains.processor.ChainProcessorTest
import com.pupscan.chains.processor.SegmentProcessorTest
import org.junit.Ignore
import org.junit.Test
import org.slf4j.LoggerFactory
import java.util.concurrent.Semaphore
import kotlin.math.roundToLong

class StepExecutorTest {
    val log = LoggerFactory.getLogger(javaClass)

    @Test
    fun idealCase() {
        test(
            1,
            10,
            Long.MAX_VALUE,
            SleepStrategy.Never(),
            ErrorStrategy.Never()
        )
    }

    @Test
    fun withTimeoutError() {
        val itemCount = 10
        var sourceTimeOutMs = 1000L
        test(
            1,
            itemCount, sourceTimeOutMs,
            SleepStrategy.FixDurationOnlyOnce(itemCount / 2, sourceTimeOutMs * 2),
            ErrorStrategy.Never()
        )
    }

    @Test
    fun withException() {
        val itemCount = 10
        test(
            1,
            itemCount, Long.MAX_VALUE,
            SleepStrategy.Never(),
            ErrorStrategy.OnlyOnce(itemCount / 2, Exception("Test error."))
        )
    }

    @Ignore("Always error not handled yet, we will retry for ever.")
    @Test
    fun withFatalErrorAtFirstStep() {
        val itemCount = 100
        var sourceTimeOutMs = 10L
        test(
            1,
            itemCount, sourceTimeOutMs,
            SleepStrategy.Never(),
            ErrorStrategy.Always(itemCount / 2, Exception("Test error."))
        )
    }

    @Test
    fun withRandomErrorAndTimeout() {
        val itemCount = 100
        var sourceTimeOutMs = 10L
        test(
            1,
            itemCount, sourceTimeOutMs,
            SleepStrategy.RandomDuration((sourceTimeOutMs * 1.3f).roundToLong()),
            ErrorStrategy.Randomly(0.1f, Exception("Fake1"), Exception("Fake2"))
        )
    }

    private fun test(
        keyCount: Int,
        itemCount: Int,
        sourceTimeoutMs: Long,
        sleepStrategy: SleepStrategy,
        errorStrategy: ErrorStrategy
    ) {

        ChainProcessor.sourceReadTimeOutMs = sourceTimeoutMs

        val keys = (1..keyCount).map { "Key$it" }
        val executor = StepExecutor()

        val observer = { processor: ChainProcessor ->
            log.debug(processor.toString())
        }

        val shutdownPermit = Semaphore(0)

        keys.forEach {
            val processor = ChainProcessor(
                "$it-step1",
                ChainProcessorTest.sourceChain(it, itemCount),
                ChainProcessorTest.targetChain(),
                SegmentProcessorTest.RandomLineAppenderProcessor(4, sleepStrategy, errorStrategy),
                observer
            )
            executor.submit(processor, VerifiedCondition(), { shutdownPermit.release() })
        }

        shutdownPermit.acquire(keyCount)
        executor.shutdownWhenDone()
        executor.awaitTermination(10)
    }
}