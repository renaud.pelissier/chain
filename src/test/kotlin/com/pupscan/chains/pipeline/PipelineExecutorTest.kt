package com.pupscan.chains.pipeline

import com.pupscan.chains.ErrorStrategy
import com.pupscan.chains.SleepStrategy
import com.pupscan.chains.chain.BufferChain
import com.pupscan.chains.chain.Chain
import com.pupscan.chains.chain.generic
import com.pupscan.chains.condition.VerifiedCondition
import com.pupscan.chains.processor.ChainProcessor
import com.pupscan.chains.processor.SegmentProcessor
import com.pupscan.chains.processor.SegmentProcessorTest
import com.pupscan.chains.TestUtils
import org.junit.Ignore
import org.junit.Test
import org.slf4j.LoggerFactory
import java.util.concurrent.Semaphore
import kotlin.math.roundToLong

class PipelineExecutorTest {

    val log = LoggerFactory.getLogger(javaClass)

    val STEP1 = "STEP1"
    val STEP2 = "STEP2"
    val steps = listOf(STEP1, STEP2)


    @Test
    fun idealCase() {
        test(10, Long.MAX_VALUE, SleepStrategy.Never(), ErrorStrategy.Never())
    }

    @Test
    fun withTimeoutError() {
        val itemCount = 10
        var sourceTimeOutMs = 1000L
        test(
            itemCount, sourceTimeOutMs,
            SleepStrategy.FixDurationOnlyOnce(itemCount / 2, sourceTimeOutMs * 2),
            ErrorStrategy.Never()
        )
    }

    @Test
    fun withException() {
        val itemCount = 10
        test(
            itemCount, Long.MAX_VALUE,
            SleepStrategy.Never(),
            ErrorStrategy.OnlyOnce(itemCount / 2, Exception("Test error.")),
            SleepStrategy.Never(),
            ErrorStrategy.Never()
        )
    }

    @Test
    fun withExceptionAtLastStep() {
        val itemCount = 10
        test(
            itemCount, Long.MAX_VALUE,
            SleepStrategy.Never(),
            ErrorStrategy.Never(),
            SleepStrategy.Never(),
            //ErrorStrategy.Always(2, Exception("Test error."))
            ErrorStrategy.OnlyOnce(2, Exception("Test error."))
        )
    }

    @Ignore("Always error not handled yet, we will retry for ever.")
    @Test
    fun withFatalErrorAtFirstStep() {
        val itemCount = 100
        var sourceTimeOutMs = 10L
        test(
            itemCount, sourceTimeOutMs,
            SleepStrategy.Never(),
            ErrorStrategy.Always(itemCount / 2, Exception("Test error.")),
            SleepStrategy.Never(),
            ErrorStrategy.Never()
        )
    }

    @Test
    fun withRandomErrorAndTimeout() {
        val itemCount = 100
        var sourceTimeOutMs = 10L
        test(
            itemCount, sourceTimeOutMs,
            SleepStrategy.RandomDuration((sourceTimeOutMs * 1.3f).roundToLong()),
            ErrorStrategy.Randomly(0.1f, Exception("Fake1"), Exception("Fake2")),
            SleepStrategy.RandomDuration((sourceTimeOutMs * 1.3f).roundToLong()),
            ErrorStrategy.Randomly(0.1f, Exception("Fake1"), Exception("Fake2"))
        )
    }

    fun test(
        itemCount: Int, sourceTimeoutMs: Long,
        sleepStrategy: SleepStrategy,
        errorStrategy: ErrorStrategy,
        sleepStrategy2: SleepStrategy = sleepStrategy,
        errorStrategy2: ErrorStrategy = errorStrategy
    ) {
        ChainProcessor.sourceReadTimeOutMs = sourceTimeoutMs

        val shutdownPermit = Semaphore(0)

        val stateObserver = { pipeline: Pipeline, step: String ->
            val processor = pipeline.processorFor(step)
            log.debug("Observing : $processor")
        }

        val endHandler = {
            shutdownPermit.release()
            log.debug("Observing : SUCCESS")
        }

        val pipelineFactory = { _: String ->
            Pipeline(
                steps,
                listOf(newChains(), newChains(), newChains()),
                listOf(
                    processor(sleepStrategy, errorStrategy),
                    processor(sleepStrategy2, errorStrategy2)
                ),
                endHandler,
                stateObserver
            )
        }

        val pipelineExecutor = PipelineExecutor()
        val id = "id"
        pipelineExecutor.begin(id, pipelineFactory(id))

        for (i in 1..itemCount)
            pipelineExecutor.add(id, "value$i")
        pipelineExecutor.end(id)

        shutdownPermit.acquire()
        pipelineExecutor.shutdown()

    }


    private fun newChains(): Chain<Any> {
        return BufferChain("end").generic()
    }

    private fun processor(
        sleepStrategy: SleepStrategy,
        errorStrategy: ErrorStrategy
    ): SegmentProcessor {
        return SegmentProcessorTest.RandomLineAppenderProcessor(4, sleepStrategy, errorStrategy)
    }
}



