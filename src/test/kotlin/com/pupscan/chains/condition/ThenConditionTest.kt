package com.pupscan.chains.condition

import org.junit.Test
import java.util.concurrent.Semaphore

class ThenConditionTest {

    @Test
    fun testConsecutiveRun() {
        val condition = VerifiedCondition() + VerifiedCondition()

        val endPermit = Semaphore(0)
        condition.runWhenVerified(Runnable { endPermit.release() })

        endPermit.acquire()

        condition.runWhenVerified(Runnable { endPermit.release() })

        endPermit.acquire()


    }
}