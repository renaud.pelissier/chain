package com.pupscan.chains.condition

import com.pupscan.chains.processor.History
import org.junit.Test
import org.slf4j.LoggerFactory
import java.util.concurrent.Semaphore

class RetryDelayConditionTest {
    private val log = LoggerFactory.getLogger(javaClass)

    @Test
    fun `Should see manually the wait interval increasing`() {

        log.debug("Starting")

        val history = History()

        val condition = RetryDelayCondition(RetryDelayCondition.Strategy.Fibonacci(history))

        var count = 0

        val endPermit = Semaphore(0)
        val runnable = object : Runnable {
            override fun run() {
                log.debug("Running")
                if (count++ > 10) {
                    endPermit.release()
                    return
                }
                history.put(System.currentTimeMillis(), 0)
                condition.runWhenVerified(this)
            }
        }
        condition.runWhenVerified(runnable)
        endPermit.acquire()
    }

}