package com.pupscan.chains.condition

import org.junit.Assert
import org.junit.Test
import java.util.concurrent.Semaphore
import java.util.concurrent.atomic.AtomicBoolean

class ExternalStateConditionTest {

    @Test
    fun test() {

        val stateValid = AtomicBoolean(false)

        val condition = object : ExternalStateCondition() {
            override fun stateIsValid(): Boolean {
                return stateValid.get()
            }
        }

        val endPermit = Semaphore(0)
        condition.runWhenVerified(Runnable { endPermit.release() })


        Assert.assertEquals(0, endPermit.availablePermits())
        stateValid.set(true)
        Assert.assertEquals(0, endPermit.availablePermits())
        condition.notifyExternalStateChange()

        endPermit.acquire()
    }
}