package com.pupscan.chains.condition

import org.junit.Test
import java.util.concurrent.Semaphore

class VerifiedConditionTest {

    @Test
    fun testConsecutiveRun() {
        val condition = VerifiedCondition()

        val endPermit = Semaphore(0)
        condition.runWhenVerified(Runnable { endPermit.release() })

        endPermit.acquire()

        condition.runWhenVerified(Runnable { endPermit.release() })

        endPermit.acquire()


    }
}