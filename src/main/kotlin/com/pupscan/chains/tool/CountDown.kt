package com.pupscan.chains.tool

import kotlin.math.max

class CountDown private constructor(private val millis: Long) {


    companion object {
        fun ofMillis(millis: Long): CountDown {
            return CountDown(millis)
        }

        fun ofSec(sec: Int): CountDown {
            return CountDown(sec * 1000L)
        }
    }

    private var chrono = Chrono()

    fun reset() {
        chrono.reset()
    }

    fun remaining(): Boolean {
        return remainingMillis() > 0
    }

    fun terminated(): Boolean {
        return !remaining()
    }

    fun remainingMillis(): Long {
        return max(millis - chrono.ellapsedMillis(), 0L)
    }

    fun remainingSec(): Int {
        return (remainingMillis() / 1000).toInt()
    }
}