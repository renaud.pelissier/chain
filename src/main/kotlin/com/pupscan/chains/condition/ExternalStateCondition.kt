package com.pupscan.chains.condition

import com.google.common.util.concurrent.ListeningExecutorService

/**
 * An simple implementation to use an external state as a Condition.
 * You must implement the logic of what is a valid state.
 * You must call notifyExternalStateChange() when the state should be checked again.
 */
abstract class ExternalStateCondition(executor: ListeningExecutorService = DEFAULT_EXECUTOR) : Condition(executor) {
    private val lock = object {}
    @Volatile
    private var registered = false

    override fun triggerOrRegister() {
        synchronized(lock) {
            if (stateIsValid()) {
                trigger()
            } else {
                registered = true
            }
        }
    }

    /**
     * Must be called when the state has change so we check stateIsValid() again.
     */
    fun notifyExternalStateChange() {
        synchronized(lock) {
            if (registered && stateIsValid()) {
                trigger()
            }
        }
    }

    override fun unregister() {
        synchronized(lock) {
            registered = false
        }
    }


    /**
     * Provide your implementation of what a valid state is
     */
    protected abstract fun stateIsValid(): Boolean
}