package com.pupscan.chains.store

interface Store<T> {

    /**
     * Will overwrite existing item
     */
    fun put(key: String, t: T)

    /**
     * Will throw a Store.NotFoundException if id is not found.
     */
    fun get(key: String): T

    /**
     * Same as get() but Will return null if id is not found.
     */
    fun find(key: String): T?

    fun delete(key: String)

    fun clear()

    /**
     * List ids by reading only the index.
     */
    fun list(): List<String>

    /**
     * Retrieve all items ordered by key.
     */
    fun getAll(): Sequence<Pair<String, T>>

    class NotFoundException(key: String) : Exception("Not entry found for key '$key'.")
}

fun <T> Store<T>.cached(): CachedStore<T> {
    return CachedStore(this)
}

/**
 * Simple implementation of a Cache.
 * Every existing values are loaded at startup with no lazy loading optimization.
 * This Cache is well suited for backing a in-memory Cache with a persisted Cache to survive reboot.
 */
class CachedStore<T>(private val store: Store<T>) : Store<T> {

    private val cache = MemoryStore<T>()

    init {
        loadCache()
    }

    private fun loadCache() {
        for ((key, value) in store.getAll()) {
            cache.put(key, value)
        }
    }

    @Synchronized
    override fun put(key: String, t: T) {
        cache.put(key, t)
        store.put(key, t)
    }

    @Synchronized
    override fun get(key: String): T {
        return cache.get(key)
    }

    @Synchronized
    override fun find(key: String): T? {
        return cache.find(key)
    }

    @Synchronized
    override fun delete(key: String) {
        cache.delete(key)
        store.delete(key)
    }

    @Synchronized
    override fun clear() {
        cache.clear()
        store.clear()
    }

    @Synchronized
    override fun list(): List<String> {
        return cache.list()
    }

    @Synchronized
    override fun getAll(): Sequence<Pair<String, T>> {
        return cache.getAll()
    }
}