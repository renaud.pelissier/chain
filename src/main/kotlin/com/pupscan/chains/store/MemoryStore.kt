package com.pupscan.chains.store

class MemoryStore<T> : Store<T> {

    private val map = LinkedHashMap<String, T>()

    override fun put(key: String, t: T) {
        map[key] = t
    }

    override fun get(key: String): T {
        return map[key] ?: throw Exception("Entry[$key] not found.")
    }

    override fun find(key: String): T? {
        return map[key]
    }

    override fun delete(key: String) {
        map.remove(key)
    }

    override fun list(): List<String> {
        return map.keys.toList()
    }

    override fun getAll(): Sequence<Pair<String, T>> {
        return map.toSortedMap().asSequence().map { Pair(it.key, it.value) }
    }

    override fun clear() {
        map.clear()
    }
}