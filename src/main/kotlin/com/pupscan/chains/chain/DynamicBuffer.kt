package com.pupscan.chains.chain

import com.google.common.util.concurrent.MoreExecutors
import com.google.common.util.concurrent.ThreadFactoryBuilder
import java.util.*
import java.util.concurrent.Executors

/**
 * A thread safe buffer that can be trimmed and closed using a magic element as the close signal.
 * The buffer can be observed. The observer will use a separated event thread to ensure the observer
 * can never block the read and write operations.
 */
class DynamicBuffer<T>(private val closingElement: T) {

    companion object {
        private val EXECUTOR by lazy {
            MoreExecutors.listeningDecorator(
                Executors.newSingleThreadExecutor(ThreadFactoryBuilder().setNameFormat("${DynamicBuffer.javaClass.simpleName}-executor-%d").build())
            )
        }
    }

    private val elements = Collections.synchronizedList(mutableListOf<T>())
    var observer = Observer.Null<T>()


    /**
     * The total number of items that were removed from the head of this buffer so far.
     */
    @Volatile
    var lastTrimmedIndex: Int = -1
        private set

    @Volatile
    var lastWriteIndex: Int = -1
        private set

    @Volatile
    var isClosed = false
        private set


    /**
     * Write a new item at the end of this queue.
     */
    @Synchronized
    fun write(t: T) {
        if (isClosed) throw IllegalStateException("Cannot add because the buffer was closed.")
        elements.add(t)
        if (t == closingElement) {
            isClosed = true
            EXECUTOR.submit { observer.ended() }
        } else {
            lastWriteIndex++
            EXECUTOR.submit { observer.written(t) }
        }
    }

    @Synchronized
    fun close() {
        write(closingElement)
    }

    @Synchronized
    private fun elementsCount(): Int {
        if (isClosed)
            return elements.size - 1
        else
            return elements.size
    }

    /**
     * Remove the n first items of the buffer and return them
     */
    @Synchronized
    fun trim(n: Int): List<T> {
        if (n <= 0) return emptyList()
        val elementsCount = elementsCount()
        if (n > elementsCount) throw IndexOutOfBoundsException("Cannot trim $n elements as it is more than the buffer size of $elementsCount.")
        val trimmedItems = mutableListOf<T>()
        for (i in 1..n)
            trimmedItems.add(elements.removeAt(0))
        lastTrimmedIndex += n
        val readOnlyTrimmedItems = trimmedItems.toList()
        EXECUTOR.submit { observer.trimmed(readOnlyTrimmedItems) }
        return readOnlyTrimmedItems
    }

    /**
     * Add n to the index of the items.
     * The result will look as if the buffer was larger by n but was also trimmed by n.
     * This function can be useful to clone a buffer that was already trimmed to reproduce the same index.
     */
    @Synchronized
    fun offset(n: Int) {
        lastTrimmedIndex += n
        lastWriteIndex += n
    }

    @Synchronized
    fun read(index: Int): T {
        if (index > lastWriteIndex) throw IndexOutOfBoundsException("Cannot read index $index as the last index is $lastWriteIndex.")
        if (index < 0) throw IndexOutOfBoundsException("Index $index is illegal, it should be positive.")
        if (index < lastTrimmedIndex) throw IndexOutOfBoundsException("Cannot read index $index because last trimmed index is greater $lastTrimmedIndex.")
        return elements[index - lastTrimmedIndex]
    }

    interface Observer<T> {
        fun written(t: T)
        fun ended()
        fun trimmed(items: List<T>)

        class Null<T> : Observer<T> {
            override fun written(t: T) {
            }

            override fun ended() {
            }

            override fun trimmed(items: List<T>) {
            }
        }
    }
}