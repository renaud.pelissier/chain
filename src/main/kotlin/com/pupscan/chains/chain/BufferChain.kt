package com.pupscan.chains.chain

/**
 * An in-memory Chain implementation based on a BlockingReadWriteBuffer.
 */
class BufferChain<T>(endElement: T) : Chain<T> {

    private val endNode = Chain.Node(-1, 0, lazy { endElement })
    private val buffer = BlockingReadWriteBuffer(endNode)

    private val recycleLock = RecycleLock(this)

    override val count
        get() = buffer.writeCount

    override val readPosition: Int
        get() = buffer.readPosition

    override val trimCount: Int
        get() = buffer.trimCount

    @Volatile
    override var lastNode: Chain.Node<T>? = null
        private set

    @Synchronized
    override fun add(originIndex: Int, originCount: Int, t: T) {
        recycleLock.ensureNotRecycled()
        val node = Chain.Node(originIndex, originCount, lazy { t })
        buffer.write(node)
        this.lastNode = node
    }

    @Synchronized
    override fun trim(count: Int): List<Chain.Node<T>> {
        return buffer.trim(count)
    }

    override fun initTrim(count: Int) {
        buffer.initTrim(count)
    }

    @Synchronized
    override fun end() {
        recycleLock.ensureNotRecycled()

        buffer.write(endNode)
    }

    @Synchronized
    override fun isEnded(): Boolean {
        return buffer.writeEnded
    }

    override fun recycle() {
        if (!recycleLock.getAndSetRecycled())
            buffer.trimAll()
    }

    override fun read(timeoutMs: Long): Chain.Node<T>? {
        recycleLock.ensureNotRecycled()
        return buffer.read(timeoutMs)
    }

    @Synchronized
    override fun readPosition(index: Int) {
        recycleLock.ensureNotRecycled()
        buffer.readPosition(index)
    }
}