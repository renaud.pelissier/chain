package com.pupscan.chains.chain

import com.pupscan.chains.tool.CountDown
import java.util.Collections
import java.util.LinkedList
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.TimeoutException

/**
 * A buffer for synchronous write and blocking read.
 * An endElement T is used as the EndOfRead token.
 * The read() will start to return null after the endElement is encountered.
 * The read position can be changed to read back again and again.
 *
 */
class BlockingReadWriteBuffer<T>(private val endElement: T) {
    private val unreadQueue = LinkedBlockingQueue<T>()
    private val readHistory = Collections.synchronizedList(LinkedList<T>())
    private val readLock = object {}
    private val writeLock = object {}

    @Volatile
    var readPosition: Int = 0
        private set

    /**
     * The total number of items that were removed from the head of this buffer so far.
     */
    @Volatile
    var trimCount: Int = 0
        private set

    @Volatile
    var writeCount: Int = 0
        private set

    @Volatile
    var readEnded = false
        private set

    @Volatile
    var writeEnded = false
        private set


    /**
     * Write a new item at the end of this queue.
     */
    fun write(t: T) {
        synchronized(writeLock) {
            if (writeEnded) throw IllegalStateException("Cannot add after writeEnded.")
            unreadQueue.put(t)
            if (t == endElement)
                writeEnded = true
            else
                writeCount++
        }
    }

    fun writeEnd() {
        write(endElement)
    }

    /**
     * Remove additional items from the head of this Buffer.
     * @return the node that where removed or null if it does not make sense (for instance for large objects not fitting
     * into memory).
     * @throws IndexOutOfBoundsException if incompatible with actual read position
     */
    fun trim(count: Int): List<T> {
        synchronized(readLock) {
            val newTrimCount = trimCount + count
            if (readPosition < newTrimCount)
                throw Chain.IllegalReadException(
                    "Attempt to trim $trimCount + $count items" +
                            " but readPosition is lower $readPosition."
                )

            val trimmed = mutableListOf<T>()
            for (i in this.trimCount until newTrimCount)
                trimmed.add(readHistory.removeAt(0))
            this.trimCount = newTrimCount
            return trimmed
        }
    }

    fun initTrim(trimCount: Int) {
        synchronized(readLock) {
            if (writeCount > 0 || writeEnded || readEnded) throw IllegalStateException("This method can only be used " +
                    "at init prior to any write or read operation.")
            writeCount = trimCount
            readPosition = writeCount
            this.trimCount = trimCount
        }
    }

    /**
     * Evict all items and set the readPosition to writeCount.
     */
    fun trimAll() {
        synchronized(readLock) {
            readPosition(writeCount)
            trim(writeCount - trimCount)
        }
    }

    /**
     * @return the next item, or null if the item is the endElement.
     * This operation is blocking until a new element is available.
     *
     * @throws TimeoutException if out of time.
     * @throws InterruptedException if interrupted while waiting
     */
    fun read(timeoutMs: Long = 60_000L): T? {
        val countDown = CountDown.ofMillis(timeoutMs)
        var t: T? = tryRead()
        while (t == null && !readEnded && countDown.remaining() && !Thread.currentThread().isInterrupted) {
            Thread.sleep(100)
            t = tryRead()
        }
        if (t == null) {
            if (readEnded)
                return null
            else if (Thread.currentThread().isInterrupted)
                throw InterruptedException("External interruption required.")
            else
                throw TimeoutException("New item not received after $timeoutMs ms.")
        }
        return t
    }

    private fun tryRead(): T? {
        synchronized(readLock) {
            if (readEnded) return null
            val t = unreadQueue.poll()

            if (t == null) return null

            readHistory.add(t)
            if (t == endElement) {
                readEnded = true
                return null
            } else {
                readPosition++
                return t
            }
        }
    }


    /**
     * Change the read position.
     * @param index will become the new readPosition. It should be lower or equal to writeCount.
     * @throws IndexOutOfBoundsException if the index is greater than the writeCount
     */
    fun readPosition(index: Int) {
        synchronized(readLock) {
            synchronized(writeLock) {
                if (index > writeCount) throw Chain.IllegalReadException("The requested readIndex $index is greater than writeCount $writeCount.")
                if (index < trimCount) throw Chain.IllegalReadException("The requested readIndex $index is lower than trimCount $trimCount.")

                readHistory += unreadQueue
                unreadQueue.clear()
                val alreadyRead = index - trimCount
                val unread = readHistory.size - alreadyRead
                for (i in 1..unread) {
                    unreadQueue.add(readHistory.removeAt(alreadyRead))
                }
                readEnded = unreadQueue.isEmpty() && writeEnded
                readPosition = index
            }
        }
    }


}