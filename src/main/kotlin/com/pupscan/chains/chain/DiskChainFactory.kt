package com.pupscan.chains.chain

import com.pupscan.chains.store.DiskStore
import com.pupscan.chains.stream.NodeStreamizer
import com.pupscan.chains.stream.Streamizer
import com.pupscan.chains.stream.StringStreamizer
import org.slf4j.LoggerFactory
import java.io.File

/**
 * Will provide LazyLoadingChain items backed by DistStore inside the given directory.
 * A utility method can be used to "clean" the directory at startup to prevent accidental accumulation
 * of headless files.
 */
class DiskChainFactory<T>(val dir: File, val streamizer: Streamizer<T>) {
    private val log = LoggerFactory.getLogger(javaClass)

    fun newChain(id: String, autoRecycle: Boolean = false): Chain<Any> {
        try {
            return tryGet(id, autoRecycle)
        } catch (e: Exception) {
            val keyDir = File(dir, id)
            log.error("Failed to load the chain from disk at '${keyDir.absolutePath}'.", e)
            File(dir, id).deleteRecursively()
            log.warn("The directory '${keyDir.absolutePath}' was deleted.")
        }

        return tryGet(id, autoRecycle)
    }

    private fun tryGet(id: String, autoRecycle: Boolean): Chain<Any> {
        val chain = LazyLoadingChain(
            DiskStore(
                File(dir, "$id-index"),
                NodeStreamizer(StringStreamizer())
            ),
            DiskStore(File(dir, "$id-data"), streamizer)
        )
        chain.autoRecycle = autoRecycle
        return chain.generic()
    }


    fun cleanJoblessDirectories(ids: List<String>) {
        dir.listFiles()?.forEach { subDir ->
            var jobFound = false
            for (id in ids) {
                if (subDir.name.startsWith(id)) {
                    jobFound = true
                    break
                }
            }
            if (!jobFound) {
                subDir.deleteRecursively()
            }
        } ?: return
    }


}