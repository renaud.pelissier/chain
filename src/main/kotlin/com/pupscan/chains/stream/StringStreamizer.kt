package com.pupscan.chains.stream

import java.io.InputStream

class StringStreamizer : Streamizer<String> {
    override fun extensions(): List<String> {
        return listOf("txt")
    }

    override fun stream(t: String): List<InputStream> {
        return listOf(t.byteInputStream())
    }

    override fun unstream(streams: List<InputStream>): String {
        return streams[0].reader().readText()
    }
}