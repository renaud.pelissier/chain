package com.pupscan.chains.stream

import com.pupscan.chains.chain.Chain
import java.io.InputStream

class NodeStreamizer<T>(private val streamizer: Streamizer<T>) :
    Streamizer<Chain.Node<T>> {
    private val delimiter = ","

    override fun extensions(): List<String> {
        return streamizer.extensions() + listOf("node")
    }

    override fun stream(t: Chain.Node<T>): List<InputStream> {
        val nodeStream = "${t.originIndex}$delimiter${t.originCount}".byteInputStream()
        return streamizer.stream(t.lazy.value) + nodeStream
    }

    override fun unstream(streams: List<InputStream>): Chain.Node<T> {
        val nodeStreamIndex = streamizer.extensions().size
        val tStreams = streams.subList(0, nodeStreamIndex)
        val nodeStream = streams[nodeStreamIndex]
        val nodeString = nodeStream.reader().readText()
        val indexes = nodeString.split(delimiter).map { it.toInt() }
        val node = streamizer.unstream(tStreams)
        return Chain.Node(indexes[0], indexes[1], lazy { node })
    }
}