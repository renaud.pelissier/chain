package com.pupscan.chains.processor

import com.pupscan.chains.chain.Chain

interface SegmentProcessor {

    /**
     * Read and consume 0 to many items from the source.
     * Return the number of consumed entries, and the list of result.
     */
    fun process(source: Chain<*>, sourceTimeoutMs: Long): Pair<Int, List<Any>>;
}