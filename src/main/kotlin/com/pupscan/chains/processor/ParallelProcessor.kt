package com.pupscan.chains.processor

import com.google.common.util.concurrent.ListenableFuture
import com.google.common.util.concurrent.ListeningExecutorService
import com.google.common.util.concurrent.MoreExecutors
import com.google.common.util.concurrent.ThreadFactoryBuilder
import com.pupscan.chains.chain.Chain
import com.pupscan.chains.tool.CountDown
import java.util.concurrent.ExecutionException
import java.util.concurrent.Executors
import java.util.concurrent.LinkedBlockingQueue

class ParallelProcessor<T, U>(
    private val innerProcessor: OneToOneProcessor<T, U>, private val nThread: Int = 4,
    protected val executor: ListeningExecutorService = defaultExecutor(nThread)
) : SegmentProcessor {


    protected val queue = LinkedBlockingQueue<ListenableFuture<U>>()

    @Suppress("UNCHECKED_CAST")
    override fun process(source: Chain<*>, sourceTimeoutMs: Long): Pair<Int, List<Any>> {
        val index = source.readPosition
        val chrono = CountDown.ofMillis(sourceTimeoutMs)

        while (queue.size < nThread && chrono.remaining()) {
            val node = source.read(chrono.remainingMillis()) ?: break
            val itemIndex = index + queue.size
            val future = executor.submit<U> { innerProcessor.process(itemIndex, node.lazy.value as T) }
            queue.add(future)
        }

        val results = mutableListOf<Any>()
        while (!queue.isEmpty()) {
            val future = queue.take()
            val result = try {
                future.get()
            } catch (e: ExecutionException) {
                throw e.cause ?: Exception("Unknown cause.")
            }
            results.add(result!!)
        }

        return results.size to results
    }

    companion object {
        fun defaultExecutor(nThread: Int): ListeningExecutorService {
            return MoreExecutors.listeningDecorator(
                Executors.newFixedThreadPool(
                    nThread,
                    ThreadFactoryBuilder().setNameFormat("parallel-processor-%d").build()
                )
            )
        }
    }
}