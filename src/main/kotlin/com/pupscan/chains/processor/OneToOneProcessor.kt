package com.pupscan.chains.processor

import com.pupscan.chains.chain.Chain

/**
 * A SegmentProcessor that consumes exactly one source item to produce one target item.
 */
@Suppress("UNCHECKED_CAST")
abstract class OneToOneProcessor<T, U>() : SegmentProcessor {


    abstract fun process(index: Int, t: T): U;

    override fun process(source: Chain<*>, sourceTimeoutMs: Long): Pair<Int, List<Any>> {
        val index = source.readPosition
        val node = source.read()
        if (node == null) return 0 to emptyList()
        val u = process(index, node.lazy.value as T)
        return 1 to listOf(u as Any)
    }
}