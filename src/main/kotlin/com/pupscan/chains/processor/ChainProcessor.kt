package com.pupscan.chains.processor

import com.google.common.util.concurrent.ListeningExecutorService
import com.pupscan.chains.chain.Chain
import com.pupscan.chains.chain.nextOriginIndex
import com.pupscan.chains.condition.Condition
import com.pupscan.chains.condition.ExternalStateCondition
import com.pupscan.chains.condition.RetryDelayCondition
import com.pupscan.chains.tool.toShortString
import org.slf4j.LoggerFactory
import java.util.concurrent.TimeoutException

/**
 * Loop over the SegmentProcessor until the source Chain is ended and fully processed.
 * The process can be interrupted if the system is terminating or if the source read
 * is blocking too long.
 * In this case, the process can be resumed by re-running the Runnable later.
 * It will resume the work from where it was interrupted using target.lastNodeSourceLastIndex.
 * @param step the unique identifier of this Processor
 * @param source where to read the input from
 * @param target where to write the produced results
 * @param segmentProcessor how to turn source items into target items
 * @param stateObserver register an observer to get notified about state or progress change
 */
class ChainProcessor(
    val step: String,
    val source: Chain<*>,
    val target: Chain<Any>,
    private val segmentProcessor: SegmentProcessor,
    private val observer: (ChainProcessor) -> Unit = { _ -> Unit },
    private val conditionExecutor: ListeningExecutorService = Condition.DEFAULT_EXECUTOR
) : Runnable {

    private val log = LoggerFactory.getLogger(javaClass)

    @Volatile
    private var isInterrupted = false

    @Volatile
    var runCount = 0

    @Volatile
    var state: State = State.WAITING
        private set
    @Volatile
    var progress = Progress()
        private set

    /**
     * A way to observe when we are exiting the Error state.
     */
    val notErrorCondition: NotErrorCondition by lazy { NotErrorCondition() }

    val history = History(20)

    val retryCondition = RetryDelayCondition(
        RetryDelayCondition.Strategy.Fibonacci(history), conditionExecutor
    )


    companion object {
        /**
         * The duration in milliseconds before the processor process is abandoned to free the thread for others.
         */
        var sourceReadTimeOutMs: Long = 20_000L
    }

    enum class State {
        WAITING,
        RUNNING,
        ERROR,
        SUCCESS
    }

    /**
     * @param completed the number of items successfully processed into the target
     * @param total the number of items currently available in the source, will change over time
     */
    class Progress(
        var runCount: Int = 0,
        val completed: Int = 0,
        val total: Int = 0,
        val exception: Exception? = null
    ) {
        override fun toString(): String {
            var errorMessage = exception?.toShortString() ?: ""
            if (!errorMessage.isEmpty()) errorMessage = "[$errorMessage]"
            return "[#$runCount][$completed/$total]$errorMessage"
        }
    }


    init {
        if (target.isEnded()) {
            state = State.SUCCESS
        }
    }

    override fun toString(): String {
        return javaClass.simpleName + "[$step][${state.name}]$progress"
    }

    override fun run() {
        runCount++
        try {
            process()
        } catch (e: Throwable) {
            setStateError(UncaughtException(e))
            throw e
        } finally {
            history.put(System.currentTimeMillis(), progress.completed)
        }
    }

    /**
     * Exit the process loop.
     */
    fun interrupt() {
        isInterrupted = true
    }

    private fun process() {
        isInterrupted = false

        if (target.isEnded()) {
            setStateSuccess()
            return
        }
        var sourceNextIndex = target.nextOriginIndex

        setStateRunning()
        try {
            var sourceConsumedToEnd = false
            while (!sourceConsumedToEnd) {
                if (Thread.currentThread().isInterrupted || isInterrupted) throw InterruptedException()

                source.readPosition(sourceNextIndex)

                val (sourceCount, results) = segmentProcessor.process(
                    source,
                    sourceReadTimeOutMs
                )

                results.forEach {
                    target.add(sourceNextIndex, sourceCount, it)
                }

                if (sourceCount > 0) {
                    updateProgressAndNotify()
                    source.trim(sourceCount)
                }

                sourceNextIndex += sourceCount

                //Note : the source.readPosition can be left anyWhere, we should only trust sourceCount
                sourceConsumedToEnd = (source.isEnded() && sourceNextIndex == source.count)

                if (Thread.currentThread().isInterrupted) throw InterruptedException()
            }
            target.end()
            setStateSuccess()
        } catch (e: Exception) {
            setStateError(e)
        } catch (e: Throwable) {
            setStateError(UncaughtException(e))
            throw e
        }
    }

    private fun setStateRunning() {
        state = State.RUNNING
        newProgressAndNotify()
    }

    private fun setStateError(e: Exception) {
        state = State.ERROR
        newProgressAndNotify(e)
    }

    private fun setStateSuccess() {
        state = State.SUCCESS
        updateProgressAndNotify()
    }

    private fun newProgressAndNotify(e: Exception? = null) {
        progress = Progress(runCount, progress.completed, progress.total, e)
        notifyChange()
    }

    private fun updateProgressAndNotify() {
        progress = Progress(runCount, source.readPosition, source.count)
        notifyChange()
    }

    private fun notifyChange() {
        observer(this)
        notErrorCondition.notifyExternalStateChange()
    }


    /**
     * A condition based on the NotError state.
     */
    inner class NotErrorCondition : ExternalStateCondition(conditionExecutor) {
        override fun stateIsValid(): Boolean {
            return state != State.ERROR
        }
    }

    class UncaughtException(cause: Throwable) : Exception(cause)
}