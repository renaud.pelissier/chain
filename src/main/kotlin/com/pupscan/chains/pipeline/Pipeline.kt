package com.pupscan.chains.pipeline

import com.pupscan.chains.chain.Chain
import com.pupscan.chains.chain.nextOriginIndex
import com.pupscan.chains.condition.Condition
import com.pupscan.chains.condition.VerifiedCondition
import com.pupscan.chains.processor.ChainProcessor
import com.pupscan.chains.processor.SegmentProcessor

class Pipeline(
    val steps: List<String>,
    private val chains: List<Chain<Any>>,
    private val segProcessors: List<SegmentProcessor>,
    private val pipelineSuccessHandler: () -> Unit = {},
    private val observer: (Pipeline, step: String) -> Unit = { _, _ -> },
    private val conditions: List<Condition> = Array<Condition>(steps.size) { _ -> VerifiedCondition() }.toList()
) {

    private val processors: List<ChainProcessor>
    private val stepsIndex = mutableMapOf<String, Int>()

    private val createProcessors: () -> List<ChainProcessor> = {
        val processors = mutableListOf<ChainProcessor>()
        for (step in steps) {
            processors.add(newChainProcessor(step))
        }
        processors
    }

    private val newChainProcessor: (String) -> ChainProcessor = { step ->
        val index = stepsIndex[step]!!
        val source = chains[index]
        val target = chains[index + 1]
        val processor = segProcessors[index]
        val observer = if (isLastStep(step))
            { chainProcessor: ChainProcessor ->
                observer(this, step)
                if (chainProcessor.state == ChainProcessor.State.SUCCESS) {
                    recycleIntermediateChains()
                    pipelineSuccessHandler()
                }
            }
        else
            { _: ChainProcessor ->
                observer(this, step)
            }

        ChainProcessor(step, source, target, processor, observer)
    }


    init {
        assert(steps.isNotEmpty())
        { "Expecting steps.isNotEmpty()." }
        assert(chains.size == steps.size + 1)
        { "Expecting chains.size == steps.size+1 but [${chains.size}] != [${steps.size}]+1" }
        assert(segProcessors.size == steps.size)
        { "Expecting processors.size == steps.size but [${segProcessors.size}] != [${steps.size}]" }
        assert(conditions.size == steps.size)
        { "Expecting conditions.size == steps.size but [${conditions.size}] != [${steps.size}]" }

        for (i in 0 until chains.lastIndex) {
            val src = chains[i]
            val target = chains[i + 1]
            assert(src.trimCount <= target.nextOriginIndex)
            { "Expecting src.trimCount < target.nextOriginIndex but [${src.trimCount}] > [${target.nextOriginIndex}]." }
        }

        steps.forEachIndexed { index, step ->
            stepsIndex[step] = index
        }
        processors = createProcessors()
    }

    fun rootChain(): Chain<Any> {
        return chains[0]
    }

    fun processorFor(step: String): ChainProcessor {
        return processors[stepIndexFor(step)]
    }

    fun previousProcessorFor(step: String): ChainProcessor? {
        return if (isFirstStep(step)) null
        else processorFor(previousStep(step))
    }

    fun conditionFor(step: String): Condition {
        return conditions[stepIndexFor(step)]
    }

    private fun stepIndexFor(step: String): Int {
        return stepsIndex[step] ?: throw Exception("Step '$step' not found.")
    }

    /**
     * Recycle all but last chain
     */
    private fun recycleIntermediateChains() {
        for (i in 0 until chains.lastIndex) {
            chains[i].recycle()
        }
    }

    fun isSuccess(): Boolean {
        return processors.last().state == ChainProcessor.State.SUCCESS
    }

    fun retryNow() {
        for (processor in processors) {
            processor.retryCondition.retryNow()
        }
    }

    fun allEmpty(): Boolean {
        for (chain in chains) {
            if (!chain.isEmpty()) return false
        }
        return true
    }

    fun isLastStep(step: String): Boolean {
        return stepIndexFor(step) == steps.size - 1
    }

    private fun isFirstStep(step: String): Boolean {
        return stepIndexFor(step) == 0
    }

    private fun previousStep(step: String): String {
        val index = steps.indexOf(step)
        if (index < 0)
            throw Exception("Unknown step '$step'.")
        else if (index == 0)
            throw Exception("Step '$step' is the first step, it has no previous step.")
        else
            return steps[index - 1]
    }

}