package com.pupscan.chains.pipeline

import com.google.common.util.concurrent.ListeningExecutorService
import com.pupscan.chains.condition.Condition
import com.pupscan.chains.condition.VerifiedCondition
import com.pupscan.chains.tool.CountDown
import org.slf4j.LoggerFactory

class PipelineExecutor(
    private val successObserver: (Pipeline) -> Unit = {},
    private val errorObserver: (Pipeline, String, Exception) -> Unit = { _, _, _ -> },
    private val conditionExecutor: ListeningExecutorService = Condition.DEFAULT_EXECUTOR
) {

    private val log = LoggerFactory.getLogger(javaClass)
    private val jobPipelines = mutableMapOf<String, Pipeline>()
    private val executors = mutableMapOf<String, StepExecutor>()


    private fun pipelineFor(id: String): Pipeline {
        return jobPipelines[id] ?: throw Exception("Pipeline not found for id $id.")
    }

    private fun executorFor(step: String): StepExecutor {
        return executors.getOrPut(step, { StepExecutor(step) })
    }

    fun begin(id: String, pipeline: Pipeline) {
        jobPipelines[id] = pipeline
        start(pipeline)
    }

    fun add(id: String, item: Any) {
        val rootChain = pipelineFor(id).rootChain()
        rootChain.add(rootChain.count, 1, item)
    }

    fun end(id: String) {
        pipelineFor(id).rootChain().end()
    }

    fun interrupt(id: String): Pipeline {
        val pipeline = jobPipelines.remove(id) ?: throw Exception("Pipeline not found for id $id.")
        for (step in pipeline.steps) {
            interrupt(pipeline, step)
        }
        return pipeline
    }

    private fun start(pipeline: Pipeline) {
        for (step in pipeline.steps) {
            submitForExecution(pipeline, step)
        }
    }

    private fun interrupt(pipeline: Pipeline, step: String) {
        val processor = pipeline.processorFor(step)
        processor.interrupt()
    }

    private fun submitForExecution(pipeline: Pipeline, step: String) {
        log.debug("Submitting Pipeline[${pipeline.hashCode()}][$step]")
        val successObserver = if (pipeline.isLastStep(step)) { ->
            successObserver(pipeline)
        } else { -> }
        val errorObserver = { e: Exception ->
            errorObserver(pipeline, step, e)
        }
        val processor = pipeline.processorFor(step)
        val previousStepNotErrorCondition =
            pipeline.previousProcessorFor(step)?.notErrorCondition ?: VerifiedCondition(conditionExecutor)
        val stepCondition = pipeline.conditionFor(step)

        executorFor(step).submit(
            processor,
            previousStepNotErrorCondition + stepCondition,
            successObserver,
            errorObserver
        )
    }

    /**
     * Blocks until both executors are shutdown.
     * It will wait for the queued Processor to finish within the timeout window.
     * It out of time, will invoke Thread.interrupt and remaining tasks will be aborted.
     */
    fun shutdown(timeoutSec: Int = Int.MAX_VALUE) {
        val countDown = CountDown.ofSec(timeoutSec)

        for ((_, executor) in executors) {
            executor.shutdownWhenDone()
        }

        for ((_, executor) in executors) {
            if (!executor.awaitTermination(countDown.remainingSec())) {
                executor.shutdownNow()
            }
        }
    }


}